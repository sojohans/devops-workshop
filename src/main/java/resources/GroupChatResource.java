package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;
import data.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     *
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path("{groupChatId}")
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId) {

        GroupChatDAO groupChatDAO = new GroupChatDAO();
        GroupChat groupChat = groupChatDAO.getGroupChat(groupChatId);

        List<Message> groupChatMessages = groupChatDAO.getGroupChatMessages(groupChatId);
        groupChat.setMessageList(groupChatMessages);

        List<User> groupChatUsers = groupChatDAO.getGroupChatUsers(groupChatId);
        groupChat.setUserList(groupChatUsers);

        return groupChat;
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat) {

        GroupChatDAO groupChatDAO = new GroupChatDAO();

        return groupChatDAO.addGroupChat(Objects.requireNonNull(groupChat));
    }

    @GET
    @Path("/user/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatByUserId(@PathParam("userId") int userId) {

        GroupChatDAO groupChatDAO = new GroupChatDAO();
        ArrayList<GroupChat> groupChats = new ArrayList<>();

        groupChats.addAll(Objects.requireNonNull(groupChatDAO.getGroupChatByUserId(userId)));

        return groupChats;
    }

    @GET
    @Path("/{groupChatId}/message")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Message> getGroupChatMessages(@PathParam("groupChatId") int groupChatId) {

        GroupChatDAO groupChatDAO = new GroupChatDAO();
        ArrayList<Message> groupChatMessages = new ArrayList<>();

        groupChatMessages.addAll(Objects.requireNonNull(groupChatDAO.getGroupChatMessages(groupChatId)));

        return groupChatMessages;
    }

    @POST
    @Path("/{groupChatId}/message")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message) {

        GroupChatDAO groupChatDAO = new GroupChatDAO();

        return groupChatDAO.addMessage(groupChatId, Objects.requireNonNull(message));
    }
}