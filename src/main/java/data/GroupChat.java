package data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class for the GroupChat object as saved in database
 */
public class GroupChat {

    private int groupChatId;
    private String groupChatName;
    private ArrayList<Message> messageList = new ArrayList<>();
    private ArrayList<User> userList = new ArrayList<>();

    public GroupChat() {
    }

    public GroupChat(int groupChatId, String groupChatName) {

        // TODO: Add input validation for groupChatId
        this.groupChatId = groupChatId;
        this.groupChatName = Objects.requireNonNull(groupChatName);
    }

    public int getGroupChatId() {
        return groupChatId;
    }

    public void setGroupChatId(int groupChatId) {

        // TODO: Add input validation for groupChatId
        this.groupChatId = groupChatId;
    }

    public String getGroupChatName() {
        return groupChatName;
    }

    public void setGroupChatName(String groupChatName) {
        this.groupChatName = Objects.requireNonNull(groupChatName);
    }

    public ArrayList<User> getUserList() {
        return new ArrayList<>(userList);
    }

    public void setUserList(List<User> userList) {
        this.userList.addAll(Objects.requireNonNull(userList));
    }

    public ArrayList<Message> getMessageList() {
        return new ArrayList<>(messageList);
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList.addAll(Objects.requireNonNull(messageList));
    }
}
