import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.*;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = "300 - 99";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = "42 * 13";
        assertEquals("546", calculatorResource.calculate(expression));


        // rounding up 16.666... -> 17
        expression = "50 / 3";
        assertEquals("17", calculatorResource.calculate(expression));

        // rounding down 8.333... -> 8
        expression = "50 / 3 / 2";
        assertEquals("8", calculatorResource.calculate(expression));

        expression = "1 + 2 + 3 + 4";
        assertEquals("10", calculatorResource.calculate(expression));

        expression = "2-3";
        assertEquals("-1",calculatorResource.calculate(expression));

        expression = "hei";
        assertEquals("Your input could not be interpreted as a mathematical expression",
                calculatorResource.calculate(expression));

    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "1+2+3+4";
        assertEquals(10 ,calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }
}
